# KeePass

## Quelques utilisations

 * Keepass est un gestionnaire de mots de passes qui permet de stocker tous ces mots de passes dans un fichier chiffré unique.


![oups](Images/logKeepass.png)

 * Il peut générer des mots de passe avec les caractères qu'on lui autorise et avec une taille choisie et les utilises en cliquant sur "Perorm Auto-Type (Ctrl-V)" :


![oups](Images/performAuto.png)

 * Pour les identifications en deux temps, comme par exemple les mails, on peut configurer dans l'onglet "Auto-Type" comment KeePass doit entrer les informations :


![oups](Images/conf.PNG)

## Avis personel

 * J'utilise KeePass parceque je le trouve assez intuitif pour une application aussi complexe. 
 * De plus, quand j'ai voulu avoir mon KeePass sur mon autre ordinateur et mon téléphone, il a juste fallu que je mettre son fichié .kdbx dans ma dropbox et d'installer l'application sur mes autres appareils.
