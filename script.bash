#!/bin/bash

time=$(date +%Y_%m_%d_%H_%M_%S)
FileToDB="save/${time}_${baseName}.sql"
FileToLog="save/${time}_${baseName}.log"

#Appel du fichier source ou sont nos variable
source $HOME/.backupconf

#Extract de la base de données et sauvegarde dans un fichier
mysqldump -u "$usr" -p"$mdp" -q --databases $baseName > $FileToDB

if [?$ = 0]
then 
    echo "All OK"
    #Remplir le fichier log avec le message de réussite et informer l'utilisateur du chemin ou est enregistrer le fichier
    echo "La base $baseName a était sauvegarder, retrouver là ici: $FileToDB" > $FileToLog

    #Garder les 5 dernier fichier et suprimmer les autre
    cd save/ && ls -t | tail -n +7 | xargs rm -r
    echo "Purge bien effectuer" > ../$FileToLog
else
    echo "NOT GOOD, RETRY"
    exit
fi

#Notifier à l'utilisateur que le script est fini
echo "script terminer"
