# int-syst

## Intégration système :

*[Sauvegarde et restauration](https://gitlab.com/DualP/int-syst/blob/master/Sauvegarde%20et%20restauration.md)

*[Gestionnaire de mot de passe](https://gitlab.com/DualP/int-syst/blob/master/Gestionnaire%20de%20mots%20de%20passe.md)

*[Projet-Infrastructure](https://gitlab.com/DualP/int-syst/blob/master/Infrastructure.md)

*[Scrip bash](https://gitlab.com/DualP/int-syst/blob/master/script.bash)